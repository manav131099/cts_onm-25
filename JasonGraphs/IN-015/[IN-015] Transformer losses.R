rm(list=ls(all =TRUE))
library(ggplot2)
library(gridExtra)
library(zoo)

pathRead <- "~/intern/Data/[712]"
setwd(pathRead)

filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2016-09-17 05:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2016-10-02 20:00:00"), format="%H:%M:%S")

date <- c()
pts <- c()
tamb <- c()
gsi <- c()
trans <- c()
date[10^6]<- pts <- tamb <- gsi <- trans[10^6] <- 0


index <- 1

for (i in filelist){
  
  temp <- read.table(i,header = T, sep = "\t")
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  temp <- temp[condition,]
  
  date[index] <- substr(i,20,29)
  pts[index] <- length(temp[,1])
  tempTamb <- as.numeric(temp[,7])
  tamb[index] <- mean(tempTamb[!is.na(tempTamb)])
  tempGsi <- as.numeric(temp[,3])
  gsi[index] <- sum(tempGsi)/60000
  
  powerlv <- as.numeric(temp[,23])
  powerhv <- as.numeric(temp[,70])
  transloss = (powerlv-powerhv)/powerlv
  transloss[abs(transloss) == Inf] <- NA
  transloss[abs(transloss) > 0.8] <- NA
  
  trans[index] <- mean(transloss[!is.na(transloss)])
  
  print(paste(i, " done"))
  index <- index + 1
}

date <- date[1:(index-1)]
pts <- pts[1:(index-1)]
da <- pts/max(pts)*100
gsi <- gsi[1:(index-1)]
tamb <- tamb[1:(index-1)]
trans <- trans[1:(index-1)]*100

result <- cbind(paste(date),da,gsi,tamb,trans)

colnames(result) <- c("date","da","gsi","tamb","trans")
rownames(result) <- NULL
result <- data.frame(result)

write.table(result,file = "C:/Users/talki/Desktop/cec intern/results/712/IN-015_trans.txt",
            row.names = FALSE,sep = "\t")
result <- read.table("~/intern/Results/[712]/1. result/IN-015_trans.txt",header = T)
result <- result[-length(result[,1]),]

result[,1] <- as.Date(result[,1],origin= result[1,1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))

result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- numeric(length(result[,5]))
result[,6][result[,2]>98] ="DA>98%"
result[,6][result[,2]<98] ="DA<98%"

result[,7] <- numeric(length(result[,5]))
result[,7][result[,3]>4] ="Gsi>4 Wh/m�"
result[,7][result[,3]<4] ="Gsi<4 Wh/m�"

mytable <- table(result[,6:7])

names(result) <- c("date","da","gsi","tamb","trans","DA","Gsi")

date <- result[,1]
datemin <- date[1]
datemax <- date[length(date)]
result[,5][result[,5] < 0] <- NA
zoo.pr1 <- zoo(result[,5], result$date)
ma1 <- rollapplyr(zoo.pr1, list(-(364:1)), mean, fill = NA, na.rm = T)
result$ambpr1.av=coredata(ma1)

graph <- ggplot(result, aes(x=date,y=trans,shape = DA, group = 1))+ylab("Transfomer Losses [%]")
final_graph <- graph + geom_point(size=1,aes(colour = Gsi))+
  theme_bw()+
  scale_y_continuous(breaks = seq(0,4,1))+
  coord_cartesian(ylim=c(0,4))+
  scale_x_date(date_breaks = "1 month",date_labels = "%b")+
  ggtitle(paste("[IN-015] Transformer Losses"), subtitle=paste0("From ",substr(date[1],1,10)," to ",substr(date[length(date)],1,10)))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size = 12, lineheight = 0.7, hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9, hjust = 0.5), panel.grid.minor = element_blank())+
  annotate("text",label = paste0("Average Transformer Losses = ", round(mean(result[,5][result[,5]>0&!is.na(result[,5])]),1),"%"),size=3.6,
           x = as.Date(date[round(0.478*length(date))]), y= 0.2)+  
  annotation_custom(tableGrob(mytable),xmin = as.numeric(datemax)-115,xmax=as.numeric(datemax)-87,ymin=3.5,ymax= 3.95)+
  geom_hline(yintercept=mean(result[,5][result[,5]>0&!is.na(result[,5])]),size=0.3)+
  annotate('text', label = '365-d moving average', x = as.Date(date[500]), y=0.45, colour = "red", size=3.6) +
  annotate("segment", x = as.Date(date[545]), xend = as.Date(date[535]), y = 0.55, yend = 1.35 , colour = "red", size=1.5, alpha=0.5, arrow=arrow()) +
  scale_shape_manual('DA',
                     labels = c('DA<98%','DA>98%'),
                     values = c(1,17), guide = guide_legend(title.position = "top"))+
  geom_line(data=result, aes(x=date, y=ambpr1.av),color="red", size=1.5) +
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm")) #top,right,bottom,left

final_graph

ggsave(final_graph,filename = "C:/Users/talki/Desktop/cec intern/results/712/[IN-015]_trans_losses.pdf",width=10,height=5)

