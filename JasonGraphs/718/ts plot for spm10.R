rm(list=ls(all =TRUE))
library(lubridate)  #datetime formatting
library(scales)
library(ggplot2)

#functions
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  library(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[718]/2018"
setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)

for(k in 2:31)
{
  z=filelist[k]
  rm(temp,ts)
  temp <- read.table(z, header =T, sep= '\t', stringsAsFactors = F)
  
  pacTemp <- temp
  #pacTemp[,1] <- ymd_hms(temp[,1])
  #smp10
  smp10 <- as.numeric(paste(abs(pacTemp[,3])))    
  
  smp10_points <- abs(smp10[smp10>5])
  
  #smp10<- abs(irrad)
  timestamps <- cbind(as.character(pacTemp[,1]),smp10)
  
  #GRID VOLTAGE
  ts <- data.frame(timestamps)
  
  print(paste(z, "done"))
  #1:length(dffg[,2])
  ts[,1] = ymd_hms(ts[,1])

  d=length(ts[,1])
  ts1 <- ggplot(data=ts, aes(x=ts[,1], y=as.numeric(as.character(ts[,2])))) + geom_point(color= "red", size = 0.9)
  ts1 <- ts1 + theme_bw() 
  ts1 <- ts1 + ggtitle(paste0("[718] Time series plot for SPM10 \n",substr(ts[1,1],1,10))) + ylab("W/m2") + xlab("Time")
  ts1 <- ts1 + theme(plot.title = element_text(hjust = 0.5))
  #ts1 <- ts1 + scale_x_datetime(breaks= date_breaks("8 hours"),
                                #labels = date_format("%H:%M"), expand = c(0,0),
                                #limits = as.POSIXct(c("00:00:00", "23:59:00"), origin="1970-01-01", tz="GMT"))
  ts1
  
  ggsave(paste0('/home/admin/Jason/cec intern/results/718/2. time series ', substr(ts[1,1],1,10),".pdf"), ts1, width =10, height=5)
}
