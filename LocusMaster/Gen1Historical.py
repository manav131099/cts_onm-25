import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

accessurl ="https://api.locusenergy.com/oauth/token"
clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
clientsecret = "7abff4f549770bc025df5eb371ce6857"
											
def authenticate():
    client_auth = requests.auth.HTTPBasicAuth(clientid, clientsecret)
    post_data = {"grant_type" : "password","username" : "shravan1994@gmail.com","password" : "welcome",
                 "client_id" : clientid,"client_secret" : clientsecret}
    												
    response = requests.post(accessurl,auth=client_auth,data=post_data)
    token_json = response.json()   							 
    return token_json["access_token"] 

def get_cols(auth,id):	
    k=[]	
    componenturl2 ="https://api.locusenergy.com/v3/components/"+str(id)+"/dataavailable"	
    while True:	
        try:	
            r = requests.get(componenturl2, headers={'Authorization': "Bearer "+auth})	
            data3=r.json()	
            for i in data3['baseFields']:	
                for j in i['aggregations']:	
                    k.append(j['shortName'])	
        except:	
            print("Columns API Failed!")	
            time.sleep(5)	
            continue	
        break 	
    print(k)	
    k=['ts']+k	
    arr=[','.join(k),k] 	
    return arr 

 
	
def bot(siteid,sitename,pytimezone,timezone,coddate,enddate):
    while True:
        try:
            a=authenticate()
        except:
            print("Authentication API Failed!")
            time.sleep(5)
            continue
        break 
    def monthdelta(date, delta):
        m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
        if not m: m = 12
        d = min(date.day, [31,
            29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
        return date.replace(day=d,month=m, year=y)
    strtmnth=str(monthdelta(datetime.datetime.now().date(),-1))
    tz = pytz.timezone(pytimezone)					 
    stations = [[] for _ in range(3)]
    siteId = siteid
    componenturl ="https://api.locusenergy.com/v3/sites/"+ str(siteId) +"/components"
    while True:
        try:
            r = requests.get(componenturl, headers={'Authorization': "Bearer "+a})
            data2=r.json()
            temp=data2['components']
        except:
            print("Component API Failed!")
            time.sleep(5)
            continue
        break  
    stationname={}
    for i in data2['components']:
        if((i['nodeType']=='METER' or i['nodeType']=='TRANSFORMER') and (i['isConceptualNode']==False) and ('LOCUS' not in i['name'].upper())):
            stations[0].append(i['id'])
            stationname[i['id']]=i['name'].strip()
            stationname[i['name'].strip()]=get_cols(a,i['id'])
        elif(i['nodeType']=='INVERTER' and (i['isConceptualNode']==False) and ('LOCUS' not in i['name'].upper())):
            stations[1].append(i['id'])
            stationname[i['id']]=i['name'].strip()
            stationname[i['name'].strip()]=get_cols(a,i['id'])
        elif(i['nodeType']=='WEATHERSTATION' and (i['isConceptualNode']==False) and ('LOCUS' not in i['name'].upper())):
            stations[2].append(i['id'])
            stationname[i['id']]=i['name'].strip()
            stationname[i['name'].strip()]=get_cols(a,i['id'])
    
    startpath="/home/admin/Start/"
    path="/home/admin/Dropbox/Gen 1 Data/"+sitename
    
    def chkdir(path):
        a=os.path.isdir(path)
        if(a):
            return
        else:
            os.makedirs(path)
            

    startdate=coddate  
    chkdir(path)
    print(("Startdate is ",startdate))
    start=startdate
    start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
    final=start
    end=datetime.datetime.strptime(enddate, "%Y-%m-%d").date()
    tot=end-start
    tot=tot.days
    historicalflag=0
    print("Bot Started!")
    for key,i in enumerate(stations):
        while True:
            try:
                a=authenticate()
            except:
                print("Authentication Failed!")
                time.sleep(5)
                continue
            break  
        for key2,j in enumerate(i):
            start=startdate
            start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
            for k in range(0,tot):
                historicalflag=1
                end=start+datetime.timedelta(days=1)
                start2=str(start)
                temppath=path+'/'+start2[0:4]+'/'+start2[0:7]
                chkdir(temppath)
                if(key==0):
                    path2=temppath+'/MFM'+'_'+str(key2+1)
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+stationname[stationname[j]][0]
                elif(key==1):
                    path2=temppath+'/INVERTER'+'_'+str(key2+1)
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+stationname[stationname[j]][0]
                else:
                    path2=temppath+'/WMS'+'_'+str(key2+1)
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+stationname[stationname[j]][0]
                while True:
                    try:
                        r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                        data4=r.json()
                        dataframe=pd.DataFrame(data4['data'])
                    except:
                        print("Historical Data API Failed!")
                        time.sleep(5)
                        continue
                    break      
                newdf=pd.DataFrame(columns=stationname[stationname[j]][1])      
                cols1=dataframe.columns.tolist()
                for i in cols1:
                    if i in stationname[stationname[j]][1]:
                        newdf[i]=dataframe[i] 
                newdf['ts']=newdf['ts'].str.replace('T',' ')
                newdf.ts=newdf.ts.str[0:19]
                if(key==0):
                    newdf.to_csv(path2+'/'+sitename+'-'+'MFM'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                elif(key==1):
                    newdf.to_csv(path2+'/'+sitename+'-'+'I'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                else:
                    newdf.to_csv(path2+'/'+sitename+'-'+'WMS'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                start=start+datetime.timedelta(days=1)
                final=start
    
    if(historicalflag==0):
        print("Historical Skipped!")
    else:
        print("Historical Done!")



siteid=sys.argv[1]
sitename=sys.argv[2]
pytimezone=sys.argv[3]
timezone=sys.argv[4]
coddate=sys.argv[5]
enddate=sys.argv[6]
bot(siteid,sitename,pytimezone,timezone,coddate,enddate)
   