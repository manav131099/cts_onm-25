rm(list=ls(all =TRUE))
library(ggplot2)
library(zoo)

args<-commandArgs(TRUE)
date=args[1]

Plot <-function(date, filename, column, gtitle)
{
  pathRead <- "/home/admin/Dropbox/Second Gen/[SG-005S]"
  pathwritetxt <- paste("/home/admin/Graphs/Graph_Extract/SG-005/[SG-005]", filename, " Graph ", date, " - PR Evolution.txt", sep="")
  graph <- paste("/home/admin/Graphs/Graph_Output/SG-005/[SG-005]", filename, " Graph ", date, " - PR Evolution.pdf", sep="")
  
  #extracting data
  setwd(pathRead)
  filelist <- dir(pattern = "([0-9]{4}).txt", recursive= TRUE)
  dff = NULL
  for(z in filelist[1:length(filelist)])
  {
    if(grepl("16",z))
  		next
    temp <- read.table(z, header = T, sep = "\t")  
    df <- temp[,c(1,4,column)]
    dff = rbind(dff, df)
  }
  names(dff)[3] <- "PR"
  dff = as.data.frame(dff)

  for(x in 1 :nrow(dff))
  {
  	if(is.finite(dff[x,3]) && (as.numeric(dff[x,3]) < 10))
  		dff[x,3] = NA
  }
  
  dff = dff[complete.cases(as.numeric(dff[,3])),]
  dff = dff[(dff[,3] <= 95),]
  dff = dff[(dff[,3] >= 0),]
  dff = dff[(as.Date(dff$Date)<= date),]
  dff <- unique(dff)
  row.names(dff) <- NULL
  dff <- tail(dff, -120)
  
  #moving average 30d
  zoo.PR <- zoo(dff[,3], as.Date(dff$Date))
  ma1 <- rollapplyr(zoo.PR, list(-(29:1)), mean, fill = NA, na.rm = T)
  dff$ambPR.av = coredata(ma1)
  row.names(dff) <- NULL
  
  firstdate <- as.Date(dff[1,1])
  lastdate <- as.Date(dff[length(dff[,1]),1])
  
  PRUse = as.numeric(dff$PR)
  PRUse = PRUse[complete.cases(PRUse)]
  last30 = round(mean(tail(PRUse,30)),1)
  last60 = round(mean(tail(PRUse,60)),1)
  last90 = round(mean(tail(PRUse,90)),1)
  last365 = round(mean(tail(PRUse,365)),1)
  lastlt = round(mean(PRUse),1)
  rightIdx = nrow(dff) * 0.6
  x2idx = round(nrow(dff) * 0.4, 0)
  
  #data sorting for visualisation
  dff$colour[dff[,2] < 2] <- '< 2'
  dff$colour[dff[,2] >= 2 & dff[,2] <= 4] <- '2 ~ 4'
  dff$colour[dff[,2] >= 4 & dff[,2] <= 6] <- '4 ~ 6'
  dff$colour[dff[,2] > 6] <- '> 6'
  dff$colour = factor(dff$colour, levels = c("< 2", "2 ~ 4", "4 ~ 6", "> 6"), labels =c('< 2', '2 ~ 4', '4 ~ 6', '> 6')) #to fix legend arrangement
  titlesettings <- theme(plot.title = element_text(face = "bold", size = 12, lineheight = 0.7, hjust = 0.5, margin = margin(0,0,7,0)), plot.subtitle = element_text   (face = "bold", size = 12, lineheight = 0.9, hjust = 0.5))

  p <- ggplot() + theme_bw()	
  p <- p + geom_point(data=dff, aes(x=as.Date(Date), y = PR, shape = as.factor(18), colour = colour), size = 2) + guides(shape = FALSE)
  p <- p + ylab("Performance Ratio [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
  p <- p + scale_x_date(date_breaks = "2 months", date_labels = "%b/%y", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
  p <- p + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13), axis.text = element_text(size=13), panel.grid.minor = element_blank(),                 panel.border = element_blank(), axis.line = element_line(colour = "black"), legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97))                     #LEGNED AT RHS
  p <- p + titlesettings + ggtitle(paste('Performance Ratio Evolution - SG-005', gtitle, sep=""), subtitle = paste0('From ', firstdate, ' to ', lastdate))
  p <- p + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
  p <- p + scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6'),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) 
  p <- p + scale_shape_manual(values = 18)
  p <- p + theme(legend.box = "horizontal", legend.direction="horizontal") #legend.position = "bottom"
  p <- p + geom_hline(yintercept = 72, colour="darkgreen", size=1) 
  p <- p + guides(colour = guide_legend(override.aes = list(shape = 18)))
  p <- p + geom_line(data=dff, aes(x=as.Date(Date), y=ambPR.av), color="red", size=1.5)
  p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.35,1]), xend = as.Date(dff[nrow(dff)* 0.38,1]), y=50, yend=50, colour="darkgreen", size=1.5)
  p <- p + annotate('text', label = "Target Budget Yield Performance Ratio [72.0%]", y = 50, x = as.Date(dff[x2idx,1]), colour = "darkgreen",fontface =2,hjust = 0)
  p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.35,1]), xend = as.Date(dff[nrow(dff)* 0.38,1]), y=45, yend=45, colour="red", size=1.5)
  p <- p + annotate('text', label = "30-d moving average of PR", y = 45, x = as.Date(dff[x2idx,1]), colour = "red",fontface =2,hjust = 0)
  p <- p + annotate('text', label = paste("Points above Target Budget PR = ",sum(dff$PR > 72.0),'/',nrow(dff),' = ',round((sum(dff$PR > 72.0)/nrow(dff))*100,1),'%',sep=''), y = 40, x = as.Date(dff[x2idx,1]), colour = "black",fontface =2,hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 30-d:", last30, "%"), y=25, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 60-d:", last60, "%"), y=20, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 90-d:", last90, "%"), y=15, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR last 365-d:", last365,"%"), y=10, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate('text', label = paste("Average PR Lifetime:", lastlt,"%"), y=5, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
  p <- p + annotate("segment", x = as.Date(dff[nrow(dff),1]), xend = as.Date(dff[nrow(dff),1]), y=0, yend=100, colour="deeppink4", size=1, alpha=0.5)
  
  ggsave(paste0(graph), p, width = 11, height=8)
  write.table(dff, na = "", pathwritetxt, row.names = FALSE, sep ="\t")
}

Plot(date, " Fab-2", 43, " - Fab-2")
Plot(date, " Fab-35", 52, " - Fab-3/5")
Plot(date, " Fab-7", 76, " - Fab-7")
Plot(date, " Fab-7G", 82, " - Fab-7G")
Plot(date, "", 35, "")