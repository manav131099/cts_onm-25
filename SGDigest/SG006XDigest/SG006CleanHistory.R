rm(list = ls())
errHandle = file('/home/admin/Logs/LogsSG006History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/SGDigest/SG006XDigest/FTPSG006Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1,1,1,1,1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins))+1))
}
NACOUNTER = c(0,0,0,0)
THRESHNA=30
checkErrata = function(row,ts,no)
{
	if(!(no==2 || no==4))
		return()
	{
  if(no == 1)
		no2="Admin"
	else if(no == 2)
		no2="Canteen"
	else if(no==3)
		no2="EM"
	else if(no==4)
		no2="Mill"
	else if(no==5)
		no2="Warehouse"
	}
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent for meter',no,'so no more mails'))
		return()
	}
	{
	if((!is.finite(as.numeric(row[2]))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		NACOUNTER[no]<<-NACOUNTER[no]+1
		if(NACOUNTER[no]<THRESHNA)
		{
			print(paste('Incremented NA values for mt',no,'but its still less than thresh'))
			return()
		}
	}
	else
	{
		NACOUNTER[no]<<-0
	}
	}
	if((!(is.finite(as.numeric(row[2])))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		FIREERRATA[no] <<- 0
		subj = paste('SG-006X',no2,'down')
		body=""
		if(NACOUNTER[no]>=THRESHNA)
			body = paste(body,"Ten consecutive NA readings detected\n")
    body = paste(body,'Last recorded timestamp and reading\n')
		body = paste(body,'Timestamp:',as.character(row[1]))
		body = paste(body,'Real Power Tot kW reading:',as.character(row[2]))
		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("SG-006X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter ',no))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		if(NACOUNTER[no]>=THRESHNA)
			msgbody = paste(msgbody,"\n",THRESHNA,"consecutive NA readings")
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "SG-006X"',sep = "")    ########## CHANGE PYTHON SCRIPT TO REFLECT TRUE OWNERS AS ARUN IS REMOVED
		system(command)
		print('Twilio message fired')
		recordTimeMaster("SG-006X","TwilioAlert",as.character(row[1]))
		NACOUNTER[no]<<-0
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
	tmstmps = as.character(dataread[,1])
	tmstmps = unlist(strsplit(tmstmps," "))
	seqa = seq(from =1 ,to=length(tmstmps),by=2)
	tmstmps2 = tmstmps[seqa]
	tmstmps = tmstmps[(seqa+1)]
	tmstmps3 = unlist(strsplit(tmstmps2,"-"))
	seqb = seq(from=1,to=length(tmstmps3),by=3)
	tmstmps3 = paste(tmstmps3[(seqb+2)],tmstmps3[(seqb)],tmstmps3[(seqb+1)],sep="-")
	tmstmps = paste(tmstmps3,tmstmps)
	dataread[,1]=as.character(tmstmps)
	{
		if(grepl("Admin",days[x]))
		{
				temp = 1
				temp2 = "Admin"
		}
		else if(grepl("Canteen",days[x]))
		{
				temp = 2
				temp2 = "Canteen"
		}
		else if(grepl("EM",days[x]))
		{
				temp = 3
				temp2 = "EM"
		}
		else if(grepl("Mill",days[x]))
		{
				temp = 4
				temp2 = "Mill"
		}
		else if(grepl("Warehouse",days[x]))
		{
				temp = 5
				temp2 = "Warehouse"
		}
	}
	indexpass = 23
	colnamesuse = colnames 
	rowtempuse = rowtemp
	if(temp==2)
	{
		colnamesuse = colnamesb
		rowtempuse = rowtempb
		indexpass = 23
	}
	if(temp==3)
	{
		colnamesuse = colnamesc
		rowtempuse = rowtempc
		indexpass = 23
	}
	if(temp==4)
	{
		colnamesuse = colnamesd
		rowtempuse = rowtempd
		indexpass = 23
	}
	if(temp==5)
	{
		colnamesuse = colnamese
		rowtempuse = rowtempe
		indexpass = 23
	}

	idxvals = match(currcolnames,colnamesuse)
	idxvals = idxvals[complete.cases(idxvals)]
  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		pathwritefinal = paste(pathwritemon,temp2,sep="/")
    checkdir(pathwritefinal)
    mtno = temp
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,temp,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2 = rowtempuse
    rowtemp2[idxvals] = dataread[y,]
		{
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnamesuse
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtempuse
        }
			FIREERRATA[temp] <<- 1
			NACOUNTER[temp] <<- 0
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
 	 }
	if(erratacheck != 0 && temp < 6)
	{
	passrow2 = df[idxts,indexpass]
	if(temp==2 || temp ==4)
		passrow2 = as.numeric(df[idxts,indexpass])
	pass = c(as.character(df[idxts,1]),as.character(passrow2))
	checkErrata(pass,idxts,mtno)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
  }
	recordTimeMaster("SG-006X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX SG006X Dump'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[SG-006X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'SG006.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('','','','','')
idxtostart = c(1,2,3,4,5)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[SG-006X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1],lastrecordeddate[2],lastrecordeddate[3],
		lastrecordeddate[4],lastrecordeddate[5])
		idxtostart[1] = match(lastrecordeddate[1],days)
		idxtostart[2] = match(lastrecordeddate[2],days)
		idxtostart[3] = match(lastrecordeddate[3],days)
		idxtostart[4] = match(lastrecordeddate[4],days)
		idxtostart[5] = match(lastrecordeddate[5],days)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
		print(paste('Date read is',lastrecordeddate[2],'and idxtostart is',idxtostart[2]))
		print(paste('Date read is',lastrecordeddate[3],'and idxtostart is',idxtostart[3]))
		print(paste('Date read is',lastrecordeddate[4],'and idxtostart is',idxtostart[4]))
		print(paste('Date read is',lastrecordeddate[5],'and idxtostart is',idxtostart[5]))
	}
}
checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,days[idxtostart[1]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
colnamesb = colnames(read.csv(paste(path,days[idxtostart[2]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesb = colnamesb[-c(1,2)]
colnamesc = colnames(read.csv(paste(path,days[idxtostart[3]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesc = colnamesc[-c(1,2)]
colnamesd = colnames(read.csv(paste(path,days[idxtostart[4]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesd = colnamesd[-c(1,2)]
colnamese = colnames(read.csv(paste(path,days[idxtostart[5]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamese = colnamesd[-c(1,2)]
rowtemp = rep(NA,(length(colnames)))
rowtempb = rep(NA,(length(colnamesb)))
rowtempc = rep(NA,(length(colnamesc)))
rowtempd = rep(NA,(length(colnamesd)))
rowtempe = rep(NA,(length(colnamese)))
x=1
stnname =  "[SG-006X-M"
days1 = days[grepl('Admin',days)]
days2 = days[grepl('Canteen',days)]
days3 = days[grepl('EM',days)]
days4 = days[grepl('Mill',days)]
days5 = days[grepl('Warehouse_',days)]

offset = c(0)
offset[1]=0
offset[2]=offset[1] + length(days1)
offset[3]=offset[2]+length(days2) #This is correct, order is Comap-01, ISI-01, Comap-02, ISI-02
offset[4]=offset[3]+length(days3)
offset[5]=offset[4]+length(days4)

{
	if(idxtostart[1] < length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
 		 stitchFile(path,days1[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if((idxtostart[2]-offset[2]) < length(days2)) ## THis is correct as Order is Comap-01, ISI-01, Comap-02, ISI-02
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in (idxtostart[2]-offset[2]) : length(days2))
		{
 		 stitchFile(path,days2[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 Done')
	}
	if((idxtostart[3]-offset[3]) < length(days3))
	{
		print(paste('idxtostart[3] is',idxtostart[3],'while length of days3 is',length(days3)))
		for(x in (idxtostart[3]-offset[3]) : length(days3))
		{
 		 stitchFile(path,days3[x],pathwrite,0)
		}
		lastrecordeddate[3] = as.character(days3[x])
		print('Meter 3 Done')
	}
	if((idxtostart[4] - offset[4])< length(days4))
	{
		print(paste('idxtostart[4] is',idxtostart[4],'while length of days4 is',length(days4)))
		for(x in (idxtostart[4]-offset[4]) : length(days4))
		{
 		 stitchFile(path,days4[x],pathwrite,0)
		}
		lastrecordeddate[4] = as.character(days4[x])
		print('Meter 4 Done')
	}
	if((idxtostart[5] - offset[5])< length(days5))
	{
		print(paste('idxtostart[5] is',idxtostart[5],'while length of days5 is',length(days5)))
		for(x in (idxtostart[5]-offset[5]) : length(days5))
		{
 		 stitchFile(path,days5[x],pathwrite,0)
		}
		lastrecordeddate[5] = as.character(days5[x])
		print('Meter 5 Done')
	}
	if( (!(idxtostart[1] < length(days1))) && (!(idxtostart[2] < length(days2))) && (!(idxtostart[3])<length(days3)) &&
	 (!(idxtostart[4] < length(days4))) && (!(idxtostart[5] < length(days5))) )
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
ERRATAFIX=0
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		ERRATAFIX= 0
		stitchFile(path,daysnew[x],pathwrite,ERRATAFIX)
		print(paste('Done',daysnew[x]))
	}
	daysnew1 = daysnew[grepl('Admin',daysnew)]
	daysnew2 = daysnew[grepl('Canteen',daysnew)]
	daysnew3 = daysnew[grepl('EM',daysnew)]
	daysnew4 = daysnew[grepl('Mill',daysnew)]
	daysnew5 = daysnew[grepl('Warehouse',daysnew)]

	if(length(daysnew1) < 1)
		daysnew1 = lastrecordeddate[1]
	if(length(daysnew2) < 1)
		daysnew2 = lastrecordeddate[2]
	if(length(daysnew3) < 1)
		daysnew3 = lastrecordeddate[3]
	if(length(daysnew4) < 1)
		daysnew4 = lastrecordeddate[4]
	if(length(daysnew5) < 1)
		daysnew5 = lastrecordeddate[5]
	write(c(as.character(daysnew1[length(daysnew1)]),as.character(daysnew2[length(daysnew2)]),
	as.character(daysnew3[length(daysnew3)]),as.character(daysnew4[length(daysnew4)]),
	as.character(daysnew5[length(daysnew5)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew1[length(daysnew1)])
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)])
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])
	lastrecordeddate[4] = as.character(daysnew4[length(daysnew4)])
	lastrecordeddate[5] = as.character(daysnew4[length(daysnew4)])
gc()
ERRATAFIX=1 #for now once GSI reading is fine change this to 1
}
print('Out of loop')
sink()
