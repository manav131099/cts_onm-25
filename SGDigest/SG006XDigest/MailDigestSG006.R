errHandle = file('/home/admin/Logs/LogsSG006XMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
Sys.sleep(120) #Add sleep to make sure SG-001X data which is used here, has time to complete
source('/home/admin/CODE/SGDigest/SG006XDigest/HistoricalAnalysis2G3GSG006.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/SGDigest/SG006XDigest/aggregateInfo.R')

getGTIData = function(date)
{
	yr=unlist(strsplit(date,"-"))
	yrmon =paste(yr[1],yr[2],sep="-")
	yr = yr[1]
	path = paste("/home/admin/Dropbox/Second Gen/[SG-001X]/",yr,"/",
	yrmon,"/[SG-001X] ",date,".txt",sep="")
	path2 = paste("/home/admin/Dropbox/Second Gen/[SG-003S]/",yr,"/",
	yrmon,"/[SG-003S] ",date,".txt",sep="")
	path3 = paste("/home/admin/Dropbox/Second Gen/[SG-004S]/",yr,"/",
	yrmon,"/[SG-004S] ",date,".txt",sep="")
	GTI=GTI2=GTI3=NA
	Tot = NA
	COV=NA
	if((!file.exists(path)) && FORCEWAIT)
		Sys.sleep(3600)
	if(file.exists(path))
	{
		dataread = read.table(path,header=T,sep="\t",stringsAsFactors=F)
		GTI = as.numeric(dataread[1,9])
	}
	if(file.exists(path2))
	{
		dataread = read.table(path2,header=T,sep="\t",stringsAsFactors=F)
		GTI2 = as.numeric(dataread[1,3])
	}
	if(file.exists(path3))
	{
		dataread = read.table(path3,header=T,sep="\t",stringsAsFactors=F)
		GTI3 = as.numeric(dataread[1,3])
	}
	irr=c(GTI,GTI2,GTI3)
	COV=round(((sdp(irr)*100)/mean(irr)),1)
	return(c(irr,COV))
}
initDigest = function(df,no)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Meter-A Admin'
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Meter-B Canteen'
	}
	else if(as.numeric(no)==3)
	{
	  no2 = 'Meter-C EM'
	}
	else if(as.numeric(no)==4)
	{
	  no2 = 'Meter-D Mill'
	}
	else if(as.numeric(no)==5)
	{
	  no2 = 'Meter-E Warehouse'
	}
	}
	ratspec = round(as.numeric(df[,2])/2624,2)
  body = "\n\n________________________________________________\n"
  body = paste(body,as.character(df[,1]),no2)
  body = paste(body,"\n________________________________________________\n\n")
  body = paste(body,"Eac",no,"-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac",no,"-2 [kWh]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nYield",no,"-1 [kWh/kWp]: ",as.character(df[,9]),sep="")
  body = paste(body,"\n\nYield",no,"-2 [kWh/kWp]: ",as.character(df[,10]),sep="")
  body = paste(body,"\n\nIrradiation (from SG-001X) [kWh/m2]: ",as.character(df[,11]),sep="")
  body = paste(body,"\n\nPR",no,"-1 [kWh/kWp]: ",as.character(df[,12]),sep="")
  body = paste(body,"\n\nPR",no,"-2 [kWh/kWp]: ",as.character(df[,13]),sep="")
  body = paste(body,"\n\nIrradiation (from SG-003S) [kWh/m2]: ",as.character(df[,14]),sep="")
  body = paste(body,"\n\nPR",no,"-1 [kWh/kWp]: ",as.character(df[,15]),sep="")
  body = paste(body,"\n\nPR",no,"-2 [kWh/kWp]: ",as.character(df[,16]),sep="")
	body = paste(body,"\n\nRatio [%]:",as.character(df[,4]))
  acpts = round(as.numeric(df[,5]) * 14.4)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,5]),"%)",sep="")
 # if(as.numeric(no)!=3)
		body = paste(body,"\n\nDowntime (%):",as.character(df[,6]))
  body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,7]))
  body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,8]))
  return(body)
}
printtsfaults = function(TS,num,body)
{
  {
	if(as.numeric(num) == 1)
	{
	  num = 'Meter-A Admin'
	}
	else if(as.numeric(num)==2)
	{
	  num = 'Meter-B Canteen'
	}
	else if(as.numeric(num)==3)
	{
	  num = 'Meter-C EM'
	}
	else if(as.numeric(num)==4)
	{
	  num = 'Meter-D Mill'
	}
	else if(as.numeric(num)==5)
	{
	  num = 'Meter-E Warehouse'
	}
	}
	# if(length(TS) > 1)
	# {
	# 	body = paste(body,"\n________________________________________________\n")
	# 	body = paste(body,paste("\nTimestamps for",num,"where Pac < 1 between 8am -5pm\n"))
	# 	for(lm in  1 : length(TS))
	# 	{
	# 		body = paste(body,TS[lm],sep="\n")
	# 	}
	# 	body = paste(body,"\n________________________________________________\n")
	# }
	# return(body)
}
sendMail = function(df1,df2,df3,df4,df5,pth1,pth2,pth3,pth4,pth5,pth6)
{

  filetosendpath = c(pth1,pth2,pth3,pth4,pth5,pth6)
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
  	GTI = getGTIData(substr(currday,14,23))
	GTI2 = as.numeric(GTI[2])
	GTI3=as.numeric(GTI[3])
	COV=as.numeric(GTI[4])
	GTI=as.numeric(GTI[1])
	data3G = read.table(pth6,header =T,sep = "\t")
	dateineed = unlist(strsplit(filenams[1]," "))
	dateineed = unlist(strsplit(dateineed[2],"\\."))
	dateineed = dateineed[1] 
	idxineed = match(dateineed,as.character(data3G[,1]))
	print('Filenames Processed')
	body=""
	body = paste(body,"Site Name: Pfizer Asia Pacific\n",sep="")
	body = paste(body,"\nLocation: Tuas South, Singapore\n")
	body = paste(body,"\nO&M Code: SG-006\n")
	body = paste(body,"\nSystem Size: 995.40 kWp\n")
	body = paste(body,"\nNumber of Energy Meters: 5\n")
	body = paste(body,"\nModule Brand / Model / Nos: 2844 / REC TP2S-350\n")
	body = paste(body,"\nInverter Brand / Model / Nos: 18 / SMA Solid-Q 50\n")
	body = paste(body,"\nSite COD: 2018-09-27\n")
	body = paste(body,"\nSystem age [days]:",DAYSALIVE,"\n")
	body = paste(body,"\nSystem age [years]:",round(DAYSALIVE/365,2))
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,as.character(data3G[idxineed,1]),"Full Site")
	body = paste(body,"\n\n________________________________________________")
	body = paste(body,"\n\nFull site generation [kWh]",data3G[idxineed,37])
	body = paste(body,"\n\nFull site yield [kWh/kWp]:",data3G[idxineed,38])
	body = paste(body,"\n\nIrradiation (From SG-001X, 1.1 km, kWh/m2):",GTI)
	body = paste(body,"\n\nFull site PR (Irradiation source SG-001X) [%]:",data3G[idxineed,39])
	body = paste(body,"\n\nIrradiation (From SG-003S, 4.7 km, kWh/m2):",GTI2)
	body = paste(body,"\n\nFull site PR (Irradiation source SG-003S) [%]:",data3G[idxineed,51])
	body = paste(body,"\n\nIrradiation (From SG-004S, 6.5 km, kWh/m2):",GTI3)
	body = paste(body,"\n\nCOV Irradiation [%]:",COV)
	irrflag=0
	if(data3G[idxineed,39]!='NA' & data3G[idxineed,39]>85){
	data3G[idxineed,39]=85
	irrflag=1
	}else if(data3G[idxineed,39]!='NA' & data3G[idxineed,39]<70 & COV>5){
	data3G[idxineed,39]=70
	irrflag=2
	}
	if(irrflag==0){
	body = paste(body,"\n\nReporting PR (From SG-001X):",data3G[idxineed,39])
	}else if(irrflag==1){
	body = paste(body,"\n\nReporting PR (From SG-001X):",data3G[idxineed,39],'>>> PR Ceiling Activated!')
	}else if(irrflag==2){
	body = paste(body,"\n\nReporting PR (From SG-001X):",data3G[idxineed,39],">>> PR Floor Activated!")
	}
	body = paste(body,"\n\nMeter Stdev/COV [%]:",data3G[idxineed,40],"/",data3G[idxineed,41],"%",sep=" ")
  body = paste(body,initDigest(df1,1))  #its correct, dont change
	body = paste(body,initDigest(df2,2))  #its correct, dont change
	body = paste(body,initDigest(df3,3))  #its correct, dont change
	body = paste(body,initDigest(df4,4))  #its correct, dont change
	body = paste(body,initDigest(df5,5))  #its correct, dont change
#   body = printtsfaults(TIMESTAMPSALARM,1,body)
#   body = printtsfaults(TIMESTAMPSALARM2,2,body)
#   body = printtsfaults(TIMESTAMPSALARM3,3,body)
#   body = printtsfaults(TIMESTAMPSALARM4,4,body)
	print('2G data processed')
	body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	print('3G data processed')
	body = gsub("\n ","\n",body)

   graph_command1 = paste("Rscript /home/admin/CODE/SGDigest/SG006XDigest/PR_Graph.r", "SG-006", 75, 0.008, "2018-10-31", substr(currday,14,23), sep=" ")
   system(graph_command1)
   graph_path1=paste('/home/admin/Graphs/Graph_Output/SG-006/[SG-006] Graph ',substr(currday,14,23),' - PR Evolution.pdf',sep="")
   graph_extract_path1=paste('/home/admin/Graphs/Graph_Extract/SG-006/[SG-006] Graph ',substr(currday,14,23),' - PR Evolution.txt',sep="")

   graph_command=paste("python3 /home/admin/CODE/SGDigest/SG006XDigest/Meter_CoV_Graph.py",substr(currday,14,23),sep=" ")
   system(graph_command,wait=TRUE)
   graph_path=paste('/home/admin/Graphs/Graph_Output/SG-006/[SG-006] Graph ',substr(currday,14,23),' - Meter CoV.pdf',sep="")
   graph_extract_path=paste('/home/admin/Graphs/Graph_Extract/SG-006/[SG-006] Graph ',substr(currday,14,23),' - Meter CoV.txt',sep="")



   filetosendpath = c(graph_path1,graph_extract_path1,graph_path,graph_extract_path,filetosendpath)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [SG-006X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls= TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F)
recordTimeMaster("SG-006X","Mail",substr(currday,14,23))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
#recipients = c('andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com', 'lucas.ferrand@cleantechsolar.com',
#'Thareth.Song@comin.com.kh','Sothea.Hin@comin.com.kh')
# recipients = getRecipients("SG-006X","m")
recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','arunsenthil@cutechgroup.com','rajasekar@cutechgroup.com','solarsg@cutechgroup.com','venkatakrishnan.shanmugampillaik@pfizer.com','amirul@cutechgroup.com','WeiBoon.Chua@pfizer.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	# recipients = getRecipients("SG-006X","m")
	recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','arunsenthil@cutechgroup.com','rajasekar@cutechgroup.com','solarsg@cutechgroup.com','venkatakrishnan.shanmugampillaik@pfizer.com','amirul@cutechgroup.com','WeiBoon.Chua@pfizer.com')
	recordTimeMaster("SG-006X","Bot")
  sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[SG-006X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
			#if(length(stations) > 2)
			#stations = c(stations[2],stations[3],stations[1])
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      pathdays3 = paste(pathmonths,stations[3],sep="/")
      pathdays4 = paste(pathmonths,stations[4],sep="/")
      pathdays5 = paste(pathmonths,stations[5],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
      writepath2Gdays4 = paste(writepath2Gmon,stations[4],sep="/")
      writepath2Gdays5 = paste(writepath2Gmon,stations[5],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      checkdir(writepath2Gdays3)
      checkdir(writepath2Gdays4)
      checkdir(writepath2Gdays5)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      days3 = dir(pathdays3)
      days4 = dir(pathdays4)
      days5 = dir(pathdays5)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
         }
				 if(grepl("Copy",c(days[t],days2[t],days3[t],days4[t],days5[t])))
				 {
				 	daycop = c(days[t],days2[t],days3[t],days4[t],days5[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]) && is.na(days3[t]) && is.na(days4[y] && is.na(days5[t])))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
				 if(!(condn1 || condn2))
				 {
				 	sendmail = 0
				 }
				 else
				 {
         	DAYSALIVE = DAYSALIVE + 1
				 }
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
				 print(paste('Processing',days3[t]))
				 print(paste('Processing',days4[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
         writepath2Gfinal4 = paste(writepath2Gdays4,"/",days4[t],sep="")
         writepath2Gfinal5 = paste(writepath2Gdays5,"/",days5[t],sep="")

         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
         readpath3 = paste(pathdays3,days3[t],sep="/")
         readpath4 = paste(pathdays4,days4[t],sep="/")
         readpath5 = paste(pathdays5,days5[t],sep="/")


				 dataprev = read.table(readpath2,header = T,sep = "\t")
				 METERCALLED <<- 1  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 2 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 METERCALLED <<- 3 # its correct dont change
         df3 = secondGenData(readpath3,writepath2Gfinal3)
				 METERCALLED <<- 4 # its correct dont change
         df4 = secondGenData(readpath4,writepath2Gfinal4)
				 METERCALLED <<- 5 # its correct dont change
         df5 = secondGenData(readpath5,writepath2Gfinal5)
				 datemtch = unlist(strsplit(days[t]," "))
		 		 thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath2Gfinal4,writepath2Gfinal5,writepath3Gfinal)

  if(sendmail ==0)
  {
		Sys.sleep(3600)
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,df3,df4,df5,writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath2Gfinal4,writepath2Gfinal5,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	FORCEWAIT = 1
	gc()
}
sink()
