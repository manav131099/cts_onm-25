rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN050Historical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN050CDigest/summaryFunctions.R')
RESETHISTORICAL=1
daysAlive = 0
#Use the Misc/IN050get column no code as reference to populate this
reorderStnPaths = c(
188,96, #WMS Entries
22,23,26,52,53,67,89,90,92,93,94,95,107,108,126,127,138,158,185,186,187,193,198,204,210,216, #MFM Entries
1,12,15,16,17,18,19,20,21,2,3,4,5,6,7,8,9,10,11,13,14,# ACH Subset
24,25, #AMB subset
27,38,45,46,47,48,49,50,51,28,29,30,31,32,33,34,35,36,37,39,40,41,42,43,44,#AMD Subset
55,59,60,61,62,63,64,65,66,56,57,58, #ANK Subset
68,79,82,83,84,85,86,87,88,69,70,71,72,73,74,75,76,77,78,80,81, # ANM Subset
97,99,100,101,102,103,104,105,106,98, #C1 Subset
110,118,119,120,121,122,123,124,125,111,112,113,114,115,116,117, #C234 Subset
129,130,131,132,133,134,135,136,137, #DS Subset
140,141,142,143, #Greygo subset
144,145,146,147, #Insp Subset
148,149,150,151,152,153, #Loom Subset
154,155, #Shed100 Subset
156,157, #SL Subset
159,170,178,179,180,181,182,183,184,160,161,162,163,164,165,166,167,168,169,171,172,173,174,175,176,177, #SSR Subset
189,190,191,192, #TP1 Subset
194,195,196,197, #TP-2 Subset
201,199,200, #Warp subset
202,203, #X12 Subset
205,206,207,208,209, #YG Subset
212,213,214,215 #YW Subset
)

if(RESETHISTORICAL)
{
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-050C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-050C]')
system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-050C]')
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-050C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-050C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-050C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-050C]"

checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 3 : length(years))
{
	pathyr = paste(path,years[x],sep="/")
	pathwriteyr = paste(pathwrite2G,years[x],sep="/")
	checkdir(pathwriteyr)
	months = dir(pathyr)
	if(!length(months))
		next
	for(y in 1 : length(months))
	{
		pathmon = paste(pathyr,months[y],sep="/")
		pathwritemon = paste(pathwriteyr,months[y],sep="/")
		checkdir(pathwritemon)
		stns = dir(pathmon)
		if(!length(stns))
			next
		stns = stns[reorderStnPaths]
		for(z in 1 : length(stns))
		{
			type = 1
			pathstn = paste(pathmon,stns[z],sep="/")
			if(grepl("MFM",stns[z]))
				type = 0
			if(grepl("WMS",stns[z]))
				type = 2
			pathwritestn = paste(pathwritemon,stns[z],sep="/")
			checkdir(pathwritestn)
			days = dir(pathstn)
			if(!length(days))
				next
			for(t in 1 : length(days))
			{
				if(z == 1)
					daysAlive = daysAlive + 1
				pathread = paste(pathstn,days[t],sep="/")
				pathwritefile = paste(pathwritestn,days[t],sep="/")
				if(RESETHISTORICAL || !file.exists(pathwritefile))
				{
					secondGenData(pathread,pathwritefile,type)
					print(paste(days[t],"Done"))
				}
			}
		}
	}
}
sink()
