source('/home/admin/CODE/common/aggregate.R')

registerMeterList("MY-006S",c("M1","M2","M3","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 44 #Column no for DA
aggColTemplate[3] = 30 #column for LastRead
aggColTemplate[4] = 31 #column for LastTime
aggColTemplate[5] = 28 #column for Eac-1
aggColTemplate[6] = 29 #column for Eac-2
aggColTemplate[7] = 25 #column for Yld-1
aggColTemplate[8] = 41 #column for Yld-2
aggColTemplate[9] = 26 #column for PR-1
aggColTemplate[10] = 42 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("MY-006S","M1",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 44 #Column no for DA
aggColTemplate[3] = 34 #column for LastRead
aggColTemplate[4] = 35 #column for LastTime
aggColTemplate[5] = 32 #column for Eac-1
aggColTemplate[6] = 33 #column for Eac-2
aggColTemplate[7] = 25 #column for Yld-1
aggColTemplate[8] = 41 #column for Yld-2
aggColTemplate[9] = 26 #column for PR-1
aggColTemplate[10] = 42 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("MY-006S","M2",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 44 #Column no for DA
aggColTemplate[3] = 38 #column for LastRead
aggColTemplate[4] = 39 #column for LastTime
aggColTemplate[5] = 36 #column for Eac-1
aggColTemplate[6] = 37 #column for Eac-2
aggColTemplate[7] = 25 #column for Yld-1
aggColTemplate[8] = 41 #column for Yld-2
aggColTemplate[9] = 26 #column for PR-1
aggColTemplate[10] = 42 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("MY-006S","M3",aggNameTemplate,aggColTemplate)
