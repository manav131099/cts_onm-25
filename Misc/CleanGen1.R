cleanGen1 = function(x)
{
path = x
years = dir(path)
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  months = dir(pathyear)
  for(y in 1 : length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    stns = dir(pathmonth)
    for(z in  1 : length(stns))
    {
      pathfinal = paste(pathmonth,stns[z],sep="/")
      days = dir(pathfinal)
      for(t in  1: length(days))
      {
        dataread = read.table(paste(pathfinal,days[t],sep="/"),header = T,sep = "\t")
        dataread = dataread[complete.cases(dataread[,1]),]
        write.table(dataread,file=paste(pathfinal,days[t],sep="/"),row.names = F,col.names = T,sep = "\t",append = F)
      }
    }
  }
}
}
