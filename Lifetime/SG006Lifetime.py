import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

tz = pytz.timezone('Asia/Singapore')
path='/home/admin/Dropbox/Third Gen/[SG-006X]'
path2='/home/admin/Dropbox/Lifetime/Gen-1/'


if(os.path.exists(path2+"SG-006-LT.txt")):
    pass
else:
    list2=[]
    for i in sorted(os.listdir(path)):
        for j in sorted(os.listdir(path+'/'+i)):
            df=pd.read_csv(path+'/'+i+'/'+j,sep='\t')
            df1 = df[['Date','GTI','LastR1','LastR2','LastR3','LastR4','LastR5','Eac12','Eac22','Eac32','Eac42','Eac52','LastT1','DA']].copy()
            df1.columns=['Date','GHI','LastR-Admin','LastR-Canteen','LastR-EM','LastR-Mill','LastR-Warehouse','Eac-Admin','Eac-Canteen','Eac-EM','Eac-Mill','Eac-Warehouse','LastT','DA']
            df1.insert(2, "GHI-Flag", 0)
            df1.insert(4, "LastR-Admin-Flag", 0)
            df1.insert(6, "LastR-Canteen-Flag", 0)   
            df1.insert(8, "LastR-EM-Flag", 0)
            df1.insert(10, "LastR-Mill-Flag", 0)
            df1.insert(12, "LastR-Warehouse-Flag", 0)
            df1.insert(14, "Eac-Admin-Flag", 0)
            df1.insert(16, "Eac-Canteen-Flag", 0)
            df1.insert(18, "Eac-EM-Flag", 0)
            df1.insert(20, "Eac-Mill-Flag", 0)
            df1.insert(22, "Eac-Warehouse-Flag", 0)
            df1['Master-Flag']=0
            if(os.path.exists(path2+"SG-006-LT.txt")):
                df1.to_csv(path2+'SG-006-LT.txt',sep='\t',mode='a',index=False,header=False)
            else:
                df1.to_csv(path2+'SG-006-LT.txt',sep='\t',mode='a',index=False,header=True)

#Historical
df2=pd.read_csv(path2+'SG-006-LT.txt',sep='\t')
startdate=(df2['Date'][len(df2['Date'])-1])
start=startdate
start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
end=datetime.datetime.now(tz).date()
tot=end-start
tot=tot.days
for n in range(tot):
    strstart=str(start)
    df=pd.read_csv(path+'/'+strstart[0:4]+'/[SG-006X] '+strstart[0:7]+'.txt',sep='\t')
    df1 = df[['Date','GTI','LastR1','LastR2','LastR3','LastR4','LastR5','Eac12','Eac22','Eac32','Eac42','Eac52','LastT1','DA']].copy()
    df1.columns=['Date','GHI','LastR-Admin','LastR-Canteen','LastR-EM','LastR-Mill','LastR-Warehouse','Eac-Admin','Eac-Canteen','Eac-EM','Eac-Mill','Eac-Warehouse','LastT','DA']
    df1.insert(2, "GHI-Flag", 0)
    df1.insert(4, "LastR-Admin-Flag", 0)
    df1.insert(6, "LastR-Canteen-Flag", 0)   
    df1.insert(8, "LastR-EM-Flag", 0)
    df1.insert(10, "LastR-Mill-Flag", 0)
    df1.insert(12, "LastR-Warehouse-Flag", 0)
    df1.insert(14, "Eac-Admin-Flag", 0)
    df1.insert(16, "Eac-Canteen-Flag", 0)
    df1.insert(18, "Eac-EM-Flag", 0)
    df1.insert(20, "Eac-Mill-Flag", 0)
    df1.insert(22, "Eac-Warehouse-Flag", 0)
    df1['Master-Flag']=0
    if((df2['Date']==strstart).any()):
        pass
    else:
        df3=df1.loc[df1['Date']==strstart]
        df3.to_csv(path2+'SG-006-LT.txt',sep='\t',mode='a',index=False,header=False)
    start=start+datetime.timedelta(days=1)

#Live
while(1):
    timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-4.5)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    print(timenowdate)
    for i in range(1,15):
        df2=pd.read_csv(path2+'SG-006-LT.txt',sep='\t')
        pastdate=timenow+datetime.timedelta(days=-i)
        pastdatestr=str(pastdate)
        df=pd.read_csv(path+'/'+pastdatestr[0:4]+'/[SG-006X] '+pastdatestr[0:7]+'.txt',sep='\t')
        df1 = df[['Date','GTI','LastR1','LastR2','LastR3','LastR4','LastR5','Eac12','Eac22','Eac32','Eac42','Eac52','LastT1','DA']].copy()
        df1.columns=['Date','GHI','LastR-Admin','LastR-Canteen','LastR-EM','LastR-Mill','LastR-Warehouse','Eac-Admin','Eac-Canteen','Eac-EM','Eac-Mill','Eac-Warehouse','LastT','DA']
        df1.insert(2, "GHI-Flag", 0)
        df1.insert(4, "LastR-Admin-Flag", 0)
        df1.insert(6, "LastR-Canteen-Flag", 0)   
        df1.insert(8, "LastR-EM-Flag", 0)
        df1.insert(10, "LastR-Mill-Flag", 0)
        df1.insert(12, "LastR-Warehouse-Flag", 0)
        df1.insert(14, "Eac-Admin-Flag", 0)
        df1.insert(16, "Eac-Canteen-Flag", 0)
        df1.insert(18, "Eac-EM-Flag", 0)
        df1.insert(20, "Eac-Mill-Flag", 0)
        df1.insert(22, "Eac-Warehouse-Flag", 0)
        df1['Master-Flag']=0
        pastdatestr2=pastdate.strftime("%d-%m-%Y")
        if(((df2['Date']==pastdatestr[0:10]) & (df2['Master-Flag'].astype(str).str[-2]=='2')).any() or ((df2['Date']==pastdatestr2[0:10]) & (df2['Master-Flag'].astype(str).str[-2]=='2')).any()):
            print('SKIPPED!')
            pass
        elif((df2['Date']==pastdatestr[0:10]).any()):
            print(pastdatestr[0:10])
            df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
            vals=df3.values.tolist()
            if(len(vals)==0):
                continue
            df2.loc[(df2['Date']==pastdatestr[0:10])|(df2['Date']==pastdatestr2[0:10]),df2.columns.tolist()]=vals[0]
            df2.to_csv(path2+'SG-006-LT.txt',sep='\t',mode='w',index=False,header=True)
        else:
            print('ABSENT HISTORY')
            df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
            if(df3.empty):
                print("empty")
            else:
                print('gg')
                df3.to_csv(path2+'SG-006-LT.txt',sep='\t',mode='a',index=False,header=False)
    df=pd.read_csv(path+'/'+timenowstr[0:4]+'/[SG-006X] '+timenowstr[0:7]+'.txt',sep='\t')
    df1 = df[['Date','GTI','LastR1','LastR2','LastR3','LastR4','LastR5','Eac12','Eac22','Eac32','Eac42','Eac52','LastT1','DA']].copy()
    df1.columns=['Date','GHI','LastR-Admin','LastR-Canteen','LastR-EM','LastR-Mill','LastR-Warehouse','Eac-Admin','Eac-Canteen','Eac-EM','Eac-Mill','Eac-Warehouse','LastT','DA']
    df1.insert(2, "GHI-Flag", 0)
    df1.insert(4, "LastR-Admin-Flag", 0)
    df1.insert(6, "LastR-Canteen-Flag", 0)   
    df1.insert(8, "LastR-EM-Flag", 0)
    df1.insert(10, "LastR-Mill-Flag", 0)
    df1.insert(12, "LastR-Warehouse-Flag", 0)
    df1.insert(14, "Eac-Admin-Flag", 0)
    df1.insert(16, "Eac-Canteen-Flag", 0)
    df1.insert(18, "Eac-EM-Flag", 0)
    df1.insert(20, "Eac-Mill-Flag", 0)
    df1.insert(22, "Eac-Warehouse-Flag", 0)
    df1['Master-Flag']=0
    if((df2['Date']==timenowdate).any()):
        print('PRESENT!')
        df3=df1.loc[df1['Date']==timenowdate]
        print(df3)
        vals=df3.values.tolist()
        df2.loc[df2['Date']==timenowdate,df2.columns.tolist()]=vals[0]
        df2.to_csv(path2+'SG-006-LT.txt',sep='\t',mode='w',index=False,header=True)
    else:
        print('ABSENT')
        df3=df1.loc[df1['Date']==timenowdate]
        if(df3.empty):
            print("Waiting")
        else:
            df3.to_csv(path2+'SG-006-LT.txt',sep='\t',mode='a',index=False,header=False)
    time.sleep(1800)


